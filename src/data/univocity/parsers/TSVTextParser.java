package data.univocity.parsers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import com.univocity.parsers.tsv.TsvParser;
import com.univocity.parsers.tsv.TsvParserSettings;

import data.model.GTEvent;

public class TSVTextParser {
	
	
	
	public static List<GTEvent> parseData(int minYear, int maxYear){
		
		TsvParserSettings settings = new TsvParserSettings();
	    //the file used in the example uses '\n' as the line separator sequence.
	    //the line separator sequence is defined here to ensure systems such as MacOS and Windows
	    //are able to process this file correctly (MacOS uses '\r'; and Windows uses '\r\n').
	    settings.getFormat().setLineSeparator("\r");
	    
	    String[] fields = {"eventid","iyear", "imonth", "iday", "nkill"};
	    settings.selectFields(fields);
	    
	    // creates a TSV parser
	    TsvParser parser = new TsvParser(settings);

	    // parses all rows in one go.
	    List<String[]> allRows = parser.parseAll(getReader("data/gtdb.tsv"));
	    List<GTEvent> listofEvents = new ArrayList<GTEvent>();
	    

	    
	    //int maxNrOfEvents = 2000;
	    
	    
	    int indexAllRows =0;
	    
	    System.out.println("allRows.size():"+allRows.size());
	    
	    for(String[] arr :allRows){
		    	if(arr[0].equals("eventid")){
		    		
		    	}
		    	//else if(k<maxNrOfEvents){
		    	else{	  		
		    		
		    		int year=0;
		    		
			    		try {
						year = Integer.parseInt(arr[1]);
					} catch (NumberFormatException e) {
						//System.err.println("NumberFormatException at indexAllRows:"+indexAllRows+" due to "+arr[1]);
						//System.err.println("NumberFormatException at indexAllRows:"+indexAllRows);
						//e.printStackTrace();
						year=0;
						//break;
					}
					
		    		if(minYear<=year&&year<=maxYear){
		    			
			    		Double d=-1.0;
						try {
							if(arr[4]!=null){
								d = new Double(arr[4]);
							}else if(arr[4]==null){
								d = 0.0;
							}
						} catch (NumberFormatException e) {
							d=-1.0;
							System.err.println("NumberFormatException due to arr[4]="+arr[4]);
							//e.printStackTrace();
						}
			    		
			    		if(d>=0.0){
				    		int integerNK = d.intValue();
				    		//TODO: define Constructor GTEvent(String[] fields, String[] arr):
				    		listofEvents.add(new GTEvent(new BigInteger(arr[0]), year, Integer.parseInt(arr[2]), Integer.parseInt(arr[3]), integerNK));	    		
			    		}
			    	}
		    	}	
//		    	}else{
//		    		break;
//		    	}
		    	indexAllRows++;
	    }
	    
	    Collections.sort(listofEvents, new Comparator<GTEvent>() {
	    	
	    	@Override
	    	public int compare(GTEvent s1, GTEvent s2) {
	            return s1.eventid.compareTo(s2.eventid);
	        }
			
			
	    });
	    
	   //System.out.println(listofEvents);
	    
	    return listofEvents;
		
	}

	public static Reader getReader(String path2File) {
		
		Reader reader=null;
		try {
			reader = new FileReader(path2File);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return reader;
	}
	
	public static void main(String[] args) {
		
		List<GTEvent> listofEvents = TSVTextParser.parseData(2000, 2001);
	    
		int sizeOfheaderIndex = 	100;
		int sizeOfcodaIndex = 100;
		
		int index=0;
		for(GTEvent event  : listofEvents){
			if(index<sizeOfheaderIndex){
				System.out.println(event.toString());
			}
			if(index==sizeOfheaderIndex){
				System.out.println(".......");
			}
			if(index>listofEvents.size()-sizeOfcodaIndex){
				System.out.println(event.toString());
			}
			index++;	
		}
		System.out.println("last index = " +index);
		System.out.println("listofEvents.size() = " +listofEvents.size());

	}

}
