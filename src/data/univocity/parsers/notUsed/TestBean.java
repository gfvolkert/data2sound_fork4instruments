/*******************************************************************************
 * Copyright 2014 uniVocity Software Pty Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package data.univocity.parsers.notUsed;

import com.univocity.parsers.annotations.*;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.tsv.TsvParser;
import com.univocity.parsers.tsv.TsvParserSettings;

import data.univocity.parsers.TSVTextParser;

import java.math.*;
import java.util.List;

public class TestBean {

	// if the value parsed in the quantity column is "?" or "-", it will be replaced by null.
	@NullString(nulls = {"?", "-"})
	// if a value resolves to null, it will be converted to the String "0".
	@Parsed(defaultNullRead = "0")
	private Integer quantity;   // The attribute type defines which conversion will be executed when processing the value.
	// In this case, IntegerConversion will be used.
	// The attribute name will be matched against the column header in the file automatically.

	@Trim
	@LowerCase
	// the value for the comments attribute is in the column at index 4 (0 is the first column, so this means fifth column in the file)
	@Parsed(index = 4)
	private String comments;

	// you can also explicitly give the name of a column in the file.
	@Parsed(field = "amount")
	private BigDecimal amount;

	@Trim
	@LowerCase
	// values "no", "n" and "null" will be converted to false; values "yes" and "y" will be converted to true
	@BooleanString(falseStrings = {"no", "n", "null"}, trueStrings = {"yes", "y"})
	@Parsed
	private Boolean pending;

	//##CLASS_END

	@Override
	public String toString() {
		return "TestBean [quantity=" + quantity + ", comments=" + comments + ", amount=" + amount + ", pending=" + pending + "]";
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Boolean getPending() {
		return pending;
	}

	public void setPending(Boolean pending) {
		this.pending = pending;
	}
	
	public static void parseCSV(){
		 // BeanListProcessor converts each parsed row to an instance of a given class, then stores each instance into a list.
	    BeanListProcessor<TestBean> rowProcessor = new BeanListProcessor<TestBean>(TestBean.class);

	    CsvParserSettings parserSettings = new CsvParserSettings();
	    parserSettings.setRowProcessor(rowProcessor);
	    parserSettings.setHeaderExtractionEnabled(true);

	    CsvParser parser = new CsvParser(parserSettings);
	    parser.parse(TSVTextParser.getReader("//Users/gfvolkert/jwspaces/e4t/topologic.text/data/csv/test_bean.txt"));

	    // The BeanListProcessor provides a list of objects extracted from the input.
	    List<TestBean> beans = rowProcessor.getBeans();
		
	}
	
	public static void parseTSV(){
		 // BeanListProcessor converts each parsed row to an instance of a given class, then stores each instance into a list.
	    BeanListProcessor<TestBean> rowProcessor = new BeanListProcessor<TestBean>(TestBean.class);

	    TsvParserSettings parserSettings = new TsvParserSettings();
	    parserSettings.setRowProcessor(rowProcessor);
	    parserSettings.setHeaderExtractionEnabled(true);

	    TsvParser parser = new TsvParser(parserSettings);
	    parser.parse(TSVTextParser.getReader("//Users/gfvolkert/jwspaces/e4t/topologic.text/data/test_bean.txt"));

	    // The BeanListProcessor provides a list of objects extracted from the input.
	    List<TestBean> beans = rowProcessor.getBeans();
	    
		
	}
	
	public static void main(String[] args) {
		parseTSV();
	}

}