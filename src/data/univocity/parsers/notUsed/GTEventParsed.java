package data.univocity.parsers.notUsed;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.univocity.parsers.annotations.Parsed;

public class GTEventParsed {
	
	//eventid, 
	//iyear, 
	//imonth, 
	//iday,
	//approxdate, extended, resolution, country, country_txt, region, region_txt, provstate, city, latitude, longitude, specificity, vicinity, location, summary, crit1, crit2, crit3, doubtterr, alternative, alternative_txt, multiple, success, suicide, attacktype1, attacktype1_txt, attacktype2, attacktype2_txt, attacktype3, attacktype3_txt, targtype1, targtype1_txt, targsubtype1, targsubtype1_txt, corp1, target1, natlty1, natlty1_txt, targtype2, targtype2_txt, targsubtype2, targsubtype2_txt, corp2, target2, natlty2, natlty2_txt, targtype3, targtype3_txt, targsubtype3, targsubtype3_txt, corp3, target3, natlty3, natlty3_txt, gname, gsubname, gname2, gsubname2, gname3, ingroup, ingroup2, ingroup3, gsubname3, motive, guncertain1, guncertain2, guncertain3, nperps, nperpcap, claimed, claimmode, claimmode_txt, claim2, claimmode2, claimmode2_txt, claim3, claimmode3, claimmode3_txt, compclaim, weaptype1, weaptype1_txt, weapsubtype1, weapsubtype1_txt, weaptype2, weaptype2_txt, weapsubtype2, weapsubtype2_txt, weaptype3, weaptype3_txt, weapsubtype3, weapsubtype3_txt, weaptype4, weaptype4_txt, weapsubtype4, weapsubtype4_txt, weapdetail, 
	//nkill, nkillus, nkillter, nwound, nwoundus, nwoundte, property, propextent, propextent_txt, propvalue, propcomment, ishostkid, nhostkid, nhostkidus, nhours, ndays, divert, kidhijcountry, ransom, ransomamt, ransomamtus, ransompaid, ransompaidus, ransomnote, hostkidoutcome, hostkidoutcome_txt, nreleased, addnotes, scite1, scite2, scite3, dbsource, INT_LOG, INT_IDEO, INT_MISC, INT_ANY, related]
	
	@Parsed(field = "eventid")
	public BigInteger eventid;
	
	@Parsed(field = "iyear")
	public Integer iyear;
	
	@Parsed(field = "imonth")
	public Integer imonth;
	
	@Parsed(field = "iday")
	public Integer iday;
	
	@Parsed(field = "nkill")
	public Double nkill;
	
	
	@Override
	public String toString() {
		return "[eventid=" + eventid + ", iyear=" + iyear + ", imonth=" + imonth + ", iday=" + iday + ", nkill=" + nkill + "]";
	}

	public BigInteger getEventid() {
		return eventid;
	}

	public void setEventid(BigInteger eventid) {
		this.eventid = eventid;
	}

	public Integer getIyear() {
		return iyear;
	}

	public void setIyear(Integer iyear) {
		this.iyear = iyear;
	}

	public Integer getImonth() {
		return imonth;
	}

	public void setImonth(Integer imonth) {
		this.imonth = imonth;
	}

	public Integer getIday() {
		return iday;
	}


	public Double getNkill() {
		return nkill;
	}

	
	
	

	
}
