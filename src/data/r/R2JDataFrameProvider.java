package data.r;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

public class R2JDataFrameProvider {

	/**
	 * DataFrame From R:
	 */
	public RList data = null;
	
	public R2JDataFrameProvider() {
		
		RConnection connection = null;

        try {
            /* Create a connection to Rserve instance running on default port
             * 6311
             */
            connection = new RConnection();

            /* Note four slashes (\\\\) in the path */
            //connection.eval("source('D:\\\\MyScript.R')");
            
            connection.eval("source('/Users/gfvolkert/R/MyScripts/data1.R')");
            //String path = "https://stat.columbia.edu/~rachel/datasets/nyt1.csv";
            
            //RList list=connection.eval("getData("+path+")").asList();
            data=connection.eval("getData()").asList();
            //String test = data.at(0).asString();
            //System.out.println("The list is=" + list);
            
        } catch (RserveException e) {
            e.printStackTrace();
        } catch (REXPMismatchException e) {
            e.printStackTrace();
        }
    
	}
	
	public static void main(String a[]) {
		
		R2JDataFrameProvider r2j = new R2JDataFrameProvider();
		try {
			
			String test = r2j.data.at(0).asString();
			System.out.println(test);
		} catch (REXPMismatchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
        
		

}
