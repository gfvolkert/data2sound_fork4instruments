package data.r;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

public class Test4Calling_R_Functions {

	public Test4Calling_R_Functions() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String a[]) {
	        
		RConnection connection = null;

	        try {
	            /* Create a connection to Rserve instance running on default port
	             * 6311
	             */
	            connection = new RConnection();

	            /* Note four slashes (\\\\) in the path */
	            //connection.eval("source('D:\\\\MyScript.R')");
	            
	            connection.eval("source('/Users/gfvolkert/R/MyScripts/MyScript.R')");
	            int num1=10;
	            int num2=20;
	            int sum=connection.eval("myAdd("+num1+","+num2+")").asInteger();
	            System.out.println("The sum is=" + sum);
	            
	        } catch (RserveException e) {
	            e.printStackTrace();
	        } catch (REXPMismatchException e) {
	            e.printStackTrace();
	        }
	    }
	

}
