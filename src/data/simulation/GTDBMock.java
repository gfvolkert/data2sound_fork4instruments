package data.simulation;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.rosuda.REngine.REXPMismatchException;

import com.univocity.parsers.annotations.Parsed;

import data.model.GTEvent;
import data.r.R2JDataFrameProvider;
import midi.abstraction.Data2Midi;

public enum GTDBMock {
	
	INSTANCE;
	
	final static int MAXIMUMNK=7;
	
	/**
	 * Days
	 */
	final static int SIZE = 300;//365;  
	final static double lambda_events_per_day = 9.0;
	final static double lambda_nk = 2.0;
	
	public List<GTEvent> genData(){
		
		List<GTEvent> list = new ArrayList<GTEvent>();
		
		for (int i = 0; i < SIZE; i++) {
			//TODO: Take into account Possion distribution:
			int eventsOnOneDay = getPoisson(lambda_events_per_day);
			System.out.println("eventsOnOneDay:"+eventsOnOneDay);
			
			int nk = 0;
			
			for (int j = 0; j < eventsOnOneDay; j++) {
				
				//TODO: Take into account Possion distribution:
				int logNK = getPoisson(lambda_nk);
				if(logNK<MAXIMUMNK){
					nk = (int) Math.exp(logNK)-1;
					System.out.println("eventsOnOneDay:"+eventsOnOneDay+"->nk:"+nk);
					//nk = logNK;
				}
				BigInteger eventid = BigInteger.valueOf(i+j);
				list.add(new GTEvent(eventid,i%365,nk));
			}
			
		}
		
		return list;
		
	}
	
	
	/**
	 * //P_{\lambda }(k)={\frac  {\lambda ^{k}}{k!}}\,{\mathrm  {e}}^{{-\lambda }}
	 * @param lambda
	 * @return
	 */
	public int getPoisson(double lambda) {
		  double L = Math.exp(-lambda);
		  double p = 1.0;
		  int k = 0;

		  do {
		    k++;
		    p *= Math.random();
		  } while (p > L);
		  return k - 1;
		}
	
	
	public static void main(String a[]) {
		
		GTDBMock.INSTANCE.genData();
		
		for (int logNK = 0; logNK < MAXIMUMNK+1; logNK++) {
			
			
			int nk = (int) Math.exp(logNK);
		
			System.out.println("logNK-->(int) Math.exp(logNK)-1:"+logNK+","+nk);
		
		}
		
	}
      
	

}
