

import java.util.Vector;

import com.cycling74.max.DataTypes;
import com.cycling74.max.MaxObject;

import data.simulation.GTDBMock;
import jm.music.data.Note;
import jm.music.data.Score;
import midi.abstraction.Data2Midi;
import midi.abstraction.NoteExtended;

//import rJava.R2JDataFrameProvider;
//import topologic.text.gtdb.GTDB;

public class Data2Sound extends MaxObject {
	
	int i=0;
//	int max = 13;
//	int nax = 16;
	
	private Vector<NoteExtended> notes = new Vector<NoteExtended>(); 
	
	//private Vector<Integer> melody = new Vector<Integer>();
	
	
	public Data2Sound(){
		
		declareOutlets(new int[]{ DataTypes.ALL, DataTypes.ALL, DataTypes.ALL});
		setInletAssist(new String[] { "bang to output"});
		setOutletAssist(new String[] { "Pitch",
          "Dynamic", "Duration"});
		
		createArray();
		//post(melody.toString());
		post("this is new...");
	}
	
	public void createArray(){
		
		
		//TODO:Lokaler Server, der bereits im Hintergrund läuft und nicht erst hier initialisiert wird:
		
		//GTDB data = new GTDB();
		//String test = data.listOfGTevnts.get(0).eventid;
				//test  = test + "s";
		
		
		//R2JDataFrameProvider r2j = new R2JDataFrameProvider();
		
		Data2Midi data2midi = new Data2Midi(GTDBMock.INSTANCE.genData());
		this.notes = data2midi.score.getPart(0).getPhrase(0).getNoteList();
		

		
		

	}
	
	@Override
	public void bang() {
		
		int max =  notes.size();
		
		if(i < max-1){
		outlet(0,notes.elementAt(i).getPitch()); 
		outlet(1,notes.elementAt(i).getDynamic()); 
		outlet(2,notes.elementAt(i).getDuration()); 
		
		i++;
		}
		else{
			i=1;
		}
	}
}
